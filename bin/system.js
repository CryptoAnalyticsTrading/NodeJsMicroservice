'use strict'; 

const Util = require('util');

/**
 * Memory leak watcher
 */
const Memwatch = require('memwatch-next');

module.exports = {

  /**
   * Function encapsulates all methods required to run prior to system startup
   */
  startup() {
    watchMemoryLeaks()
  }

}

/**
 * Function listening to RAM increased usage
 */ 
function watchMemoryLeaks() {
  let hd = null;
  Memwatch.on('leak', (info) => {
    console.log('memwatch::leak');
    console.error(info);
    if (!hd) 
      hd = new Memwatch.HeapDiff();
    else {
      const diff = hd.end();
      
      console.error(Util.inspect(diff, true, null));
      hd = null;
    }
  });
}