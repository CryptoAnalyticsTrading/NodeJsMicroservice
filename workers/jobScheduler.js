'use strict'; 

/**
 * BiArbitrage job
 */
const BarWorkerTaskQue = require('./jobs/barJob/barWorker')
const FooWorkerTaskQue = require('./jobs/fooJob/fooWorker')

module.exports = {
  /**
   * Method handles creation of new Foo jobs
   * 
   * @param {str} jobName
   * @param {str} jobDescription
   */
  scheduleFooJob(job, minutesTimeout) {

    // Configure settings for repeating jobs
    let cronConfig = {  
      repeat: {
        cron: `*/${minutesTimeout} * * * *`
      }
    }

    // Schedule a job to run
    FooWorkerTaskQue.add(job, cronConfig)
  },

  /**
   * Method handles creation of new Foo jobs
   * 
   * @param {str} jobName
   * @param {str} jobDescription
   */
  scheduleBarJob(job, minutesTimeout) {

    // Configure settings for repeating jobs
    let cronConfig = {  
      repeat: {
        cron: `*/${minutesTimeout} * * * *`
      }
    }

    // Schedule a job to run
    BarWorkerTaskQue.add(job, cronConfig)
  }
}