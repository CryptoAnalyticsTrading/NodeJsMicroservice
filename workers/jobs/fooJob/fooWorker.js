'use strict';

// Queue job scheduler
const Queue = require('bull')
const FooWorker = new Queue('FooWorker')

// Logging
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOG_LEVEL || 'info';

/**
 * Job logic
 */
FooWorker.process(async (job, done) => {
  const method_name = "FooWorker.process"

  logger.info(`[${method_name}] ---- Job started  ${job.id}`)
  logger.info(`[${method_name}]      Job name     ${job.data.name}`)
  logger.info(`[${method_name}]      Job data     ${job.data.data}`)
  logger.info(`[${method_name}] ---- Job finished ${job.id}`)
  
  done(42)
})

module.exports = FooWorker