'use strict';

/**
 * Foo worker data model
 */
function FooWorkerDataModel () {
  this.name = "FooWorkerDataModel"
  this.data = []
}

// Only allow to change existing values
Object.seal(FooWorkerDataModel)

module.exports = FooWorkerDataModel