'use strict';

/**
 * Bar worker data model
 */
function BarWorkerDataModel() {
  this.name = "BarWorkerDataModel"
  this.data = []
}

// Only allow to change existing values
Object.seal(BarWorkerDataModel)

module.exports = BarWorkerDataModel