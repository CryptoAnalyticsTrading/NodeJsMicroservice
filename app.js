/******************************************
 * 
 * Nodejs microservice template application
 * 
 *****************************************/
'use strict';

// Application infra
const System = require('./bin/system');

// Config
const Config = require('config.json')('./config/app/application.json');   

// Logging & environment
const env = process.env.NODE_ENV || 'dev';
const port = process.env.PORT || 3000;

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOG_LEVEL || 'info';

// Fault tolerance
const Errorhandler = require('errorhandler');

// Workers
const JobScheduler = require('./workers/jobScheduler');

// Workers data models
const BarWorkerDm =  require('./workers/jobs/barJob/barWorkerDataModel');
const FooWorkerDm =  require('./workers/jobs/fooJob/fooWorkerDataModel');

(function(){

	logger.info(`**********************************`)
	logger.info(`FooBar NodeJs microservice started`)
	logger.info(`**********************************`)

	// Run system startup tasks handler
	System.startup();

	// Fake data 
	let barWorkerDm = new BarWorkerDm()
	barWorkerDm.data.push(1)
	barWorkerDm.data.push(2)
	barWorkerDm.data.push(3)

	let fooWorkerDm = new FooWorkerDm()
	fooWorkerDm.data.push(4)
	fooWorkerDm.data.push(5)
	fooWorkerDm.data.push(6)

	// Schedule jobs for workers
	JobScheduler.scheduleBarJob(barWorkerDm, 1)
	JobScheduler.scheduleFooJob(fooWorkerDm, 1)

})();